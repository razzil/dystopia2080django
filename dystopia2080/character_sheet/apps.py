from django.apps import AppConfig


class CharacterSheetConfig(AppConfig):
    name = "dystopia2080.character_sheet"
