from django.db import models

from dystopia2080.users.models import User


class Stat(models.Model):
    name = models.CharField(max_length=255, unique=True)
    description = models.TextField(blank=True)

    @property
    def short(self) -> str:
        return self.name[:3].upper()

    class Meta:
        ordering = ["name"]

    def __str__(self) -> str:
        return self.name


class Skill(models.Model):
    name = models.CharField(max_length=255, unique=True)
    stat = models.ForeignKey(Stat, on_delete=models.CASCADE)
    description = models.TextField(blank=True)

    class Meta:
        ordering = ["name"]

    def __str__(self) -> str:
        return self.name


class Character(models.Model):
    name = models.CharField(max_length=255, unique=True)
    user = models.ForeignKey(
        User,
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
        default=None,
        related_name="character",
    )
    unspent_ip = models.IntegerField(default=0)
    unspent_cp = models.IntegerField(default=0)

    class Meta:
        ordering = ["name"]

    def __str__(self) -> str:
        return self.name


class CharacterSkill(models.Model):
    character = models.ForeignKey(
        Character, on_delete=models.CASCADE, related_name="skills"
    )
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)
    value = models.IntegerField(default=-1)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["character", "skill"], name="unique-character-skill"
            )
        ]

    def __str__(self) -> str:
        return "{} {} {}".format(self.character, self.skill, self.value)


class CharacterStat(models.Model):
    character = models.ForeignKey(
        Character, on_delete=models.CASCADE, related_name="stats"
    )
    stat = models.ForeignKey(Stat, on_delete=models.CASCADE)
    value = models.IntegerField()
    unspent_ip = models.IntegerField(default=0)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["character", "stat"], name="unique-character-stat"
            )
        ]

    def __str__(self) -> str:
        return "{} {} {}".format(self.character, self.stat.short, self.value)
