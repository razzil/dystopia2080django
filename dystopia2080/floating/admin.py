from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from .models import FloatingText


class FloatingAdmin(SummernoteModelAdmin):
    summernote_fields = ("content",)
    list_display = ("name", "slug")
    list_filter = ("name", "slug")
    search_fields = ("name", "content")
    prepopulated_fields = {"slug": ("name",)}


admin.site.register(FloatingText, FloatingAdmin)
