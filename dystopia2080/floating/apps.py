from django.apps import AppConfig


class FloatingConfig(AppConfig):
    name = "dystopia2080.floating"
