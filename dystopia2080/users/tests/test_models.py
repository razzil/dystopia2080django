import pytest

from dystopia2080.users.models import User

pytestmark = pytest.mark.django_db


def test_user_get_absolute_url(user: User):
    assert user.get_absolute_url() == "/users/{}/".format(user.username)
