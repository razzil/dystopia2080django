from django.apps import AppConfig


class TimelineConfig(AppConfig):
    name = "dystopia2080.timeline"
