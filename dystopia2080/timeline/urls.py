from django.urls import path

from dystopia2080.timeline import views

urlpatterns = [
    path("", views.post_index, name="timeline"),
]
