from django.shortcuts import get_object_or_404, render

from .models import NPC


def npc_index(request):
    npc = NPC.objects.filter(status=True)
    context = {
        "npcs": npc,
    }
    return render(request, "npc/index.html", context)


def npc_detail(request, slug):
    npc = get_object_or_404(NPC, slug=slug)

    return render(
        request,
        "npc/detailed.html",
        {
            "npc": npc,
        },
    )
