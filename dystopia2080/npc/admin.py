from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from .models import NPC


class PostAdmin(SummernoteModelAdmin):
    summernote_fields = ("content",)
    list_display = ("name", "slug", "status", "publication_date")
    list_filter = ("status", "publication_date")
    search_fields = ("title", "content")
    prepopulated_fields = {"slug": ("name",)}


admin.site.register(NPC, PostAdmin)
