from django.shortcuts import get_object_or_404, render

from .models import Story


def post_index(request):
    posts = Story.objects.filter(status=True)
    context = {
        "stories": posts,
    }
    return render(request, "story/index.html", context)


def post_detail(request, slug):
    template_name = "story/detailed.html"
    post = get_object_or_404(Story, slug=slug)

    return render(
        request,
        template_name,
        {
            "post": post,
        },
    )
