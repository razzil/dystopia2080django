from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from .models import Story


class PostAdmin(SummernoteModelAdmin):
    summernote_fields = ("content",)
    list_display = ("title", "slug", "status", "story_date", "publication_date")
    list_filter = ("status", "publication_date")
    search_fields = ("title", "content")
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(Story, PostAdmin)
