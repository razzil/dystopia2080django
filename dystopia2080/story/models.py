import re

from django.db import models

from dystopia2080.users.models import User

STATUS = (
    (0, "Draft"),
    (1, "Public"),
)


class Story(models.Model):
    title = models.CharField(max_length=255, unique=True)
    story_date = models.DateField()
    slug = models.SlugField(max_length=255, unique=True)
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="blog_posts"
    )
    last_modified = models.DateTimeField(auto_now=True)
    summary = models.TextField(max_length=200, blank=True, help_text="Optional summary")
    content = models.TextField()
    publication_date = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=STATUS, default=0)

    class Meta:
        ordering = ["-story_date", "-title"]

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("story-detail", kwargs={"slug": str(self.slug)})

    @property
    def summary_generated(self) -> str:
        summary = str(self.summary)
        if summary:
            return summary
        text = str(self.content)

        regex = re.compile(r"(<[^>]+>)")
        text = regex.sub("", text)

        return " ".join(text.split(" ")[:50]) + "..."
