from django.shortcuts import get_object_or_404, render

from dystopia2080.floating.models import FloatingText

from .models import Action, Class


def index(request):
    context = {
        "actions": Action.objects.filter(status=True),
        "classes": Class.objects.filter(status=True),
    }
    return render(request, "rules/base.html", context)


def action(request, slug):
    context = {
        "actions": Action.objects.filter(status=True),
        "classes": Class.objects.filter(status=True),
        "content": get_object_or_404(Action, slug=slug),
    }
    return render(request, "rules/detail.html", context)


def class_(request, slug):
    context = {
        "actions": Action.objects.filter(status=True),
        "classes": Class.objects.filter(status=True),
        "content": get_object_or_404(Class, slug=slug),
    }
    return render(request, "rules/detail.html", context)


def level_up(request):
    try:
        content = FloatingText.objects.get(slug="level_up")
    except FloatingText.DoesNotExist:
        content = None
    context = {
        "actions": Action.objects.filter(status=True),
        "classes": Class.objects.filter(status=True),
        "content": content,
    }
    return render(request, "rules/detail.html", context)


def perks(request):
    try:
        content = FloatingText.objects.get(slug="perks")
    except FloatingText.DoesNotExist:
        content = None
    context = {
        "actions": Action.objects.filter(status=True),
        "classes": Class.objects.filter(status=True),
        "content": content,
    }
    return render(request, "rules/detail.html", context)
