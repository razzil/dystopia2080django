from django.db import models

STATUS = (
    (0, "Draft"),
    (1, "Public"),
)


class Class(models.Model):
    name = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(max_length=255, unique=True)
    last_modified = models.DateTimeField(auto_now=True)
    content = models.TextField()
    publication_date = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=STATUS, default=0)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("class-detail", kwargs={"slug": str(self.slug)})


class Action(models.Model):
    name = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(max_length=255, unique=True)
    last_modified = models.DateTimeField(auto_now=True)
    content = models.TextField()
    publication_date = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=STATUS, default=0)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("action-detail", kwargs={"slug": str(self.slug)})
