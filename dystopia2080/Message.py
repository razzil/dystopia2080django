class Message:
    def __init__(self, message: str, tags: str = "") -> None:
        self.message = message
        self.tags = tags

    def __str__(self) -> str:
        return self.message
