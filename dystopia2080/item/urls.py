from django.urls import path

from . import views

urlpatterns = [
    path("", views.item_index, name="items"),
    path("a/<slug:slug>", views.armor_detail, name="armor"),
    path("w/<slug:slug>", views.weapon_detail, name="weapon"),
]
