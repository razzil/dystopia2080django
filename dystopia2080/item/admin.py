from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from .models import Ammunition, Armor, ArmorSlot, ArmorType, DamageType, Rarity, Weapon


class ArmorAdmin(SummernoteModelAdmin):
    summernote_fields = ("additional_effects", "strain_effects")
    list_display = ("name", "slug", "status", "publication_date")
    list_filter = ("status", "publication_date", "rarity", "slot")
    search_fields = ("title", "additional_effects", "strain_effects")
    prepopulated_fields = {"slug": ("name",)}


class WeaponAdmin(SummernoteModelAdmin):
    summernote_fields = ("hit_table", "miscellaneous", "range_table")
    list_display = ("name", "slug", "status", "publication_date")
    list_filter = (
        "ammunition",
        "ammunition_count",
        "ap",
        "max_range",
        "pierce",
        "publication_date",
        "rarity",
        "status",
    )
    search_fields = ("critical_effect", "miscellaneous", "title")
    prepopulated_fields = {"slug": ("name",)}


class NameAdmin(SummernoteModelAdmin):
    list_display = ("name",)
    list_filter = ("name",)


admin.site.register(Ammunition, NameAdmin)
admin.site.register(Armor, ArmorAdmin)
admin.site.register(ArmorSlot, NameAdmin)
admin.site.register(ArmorType, NameAdmin)
admin.site.register(DamageType, NameAdmin)
admin.site.register(Rarity, NameAdmin)
admin.site.register(Weapon, WeaponAdmin)
