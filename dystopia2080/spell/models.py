from django.db import models

STATUS = (
    (0, "Draft"),
    (1, "Public"),
)


class Spell(models.Model):
    name = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(max_length=255, unique=True)
    icon = models.ImageField(blank=True, null=True)
    tier = models.IntegerField()
    energy = models.IntegerField()
    cool_down = models.IntegerField()
    ki = models.IntegerField()
    ap = models.IntegerField()
    requires = models.CharField(max_length=255)

    effect = models.TextField()
    description = models.TextField()

    last_modified = models.DateTimeField(auto_now=True)
    publication_date = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=STATUS, default=0)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name
