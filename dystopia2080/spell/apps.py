from django.apps import AppConfig


class SpellConfig(AppConfig):
    name = "dystopia2080.spell"
