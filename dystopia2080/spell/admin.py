from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from .models import Spell


class PostAdmin(SummernoteModelAdmin):
    summernote_fields = ("description", "effect")
    list_display = (
        "name",
        "status",
        "publication_date",
        "tier",
        "ap",
        "ki",
        "energy",
        "cool_down",
        "requires",
    )
    list_filter = (
        "status",
        "publication_date",
        "tier",
        "ap",
        "ki",
        "energy",
        "cool_down",
        "requires",
    )
    search_fields = ("name", "description", "effect")
    prepopulated_fields = {"slug": ("name",)}


admin.site.register(Spell, PostAdmin)
