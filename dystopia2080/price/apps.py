from django.apps import AppConfig


class PriceConfig(AppConfig):
    name = "dystopia2080.price"
