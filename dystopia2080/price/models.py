from django.db import models

from dystopia2080.users.models import User

STATUS = (
    (0, "Draft"),
    (1, "Public"),
)


class Prices(models.Model):
    name = models.CharField(max_length=255, unique=True)
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="character_prices"
    )
    last_modified = models.DateTimeField(auto_now=True)
    base = models.IntegerField()
    min = models.IntegerField()
    publication_date = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=STATUS, default=0)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name
