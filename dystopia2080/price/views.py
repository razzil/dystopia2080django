from django.shortcuts import render

from .models import Prices


def index(request):
    prices = Prices.objects.filter(status=True)
    context = {
        "prices": prices,
    }
    return render(request, "price/index.html", context)
