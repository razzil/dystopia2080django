# syntax=docker/dockerfile:1
FROM python:3.8

ENV PYTHONUNBUFFERED=1

WORKDIR /code

COPY requirements/base.txt /code/base.txt
COPY requirements/local.txt /code/requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

#COPY . /code/
